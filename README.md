# outils pour la classe



## Présentation

Outils réalisés pour des actions précises dans la classe. 
PAr exemple: un convertisseur d'unités qui permet par un clic de souris de voir où se déplace la virgule lorsqu'on veut changer d'unité de mesure dans un tableau de conversion.
Il s'agit d'un fichier lisible par le navigateur web. Donc, peut être utilisé au vidéo prijecteur comme sur des tablettes.




## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.


## License
Projet sous licence Creative common BY-NC-SA

